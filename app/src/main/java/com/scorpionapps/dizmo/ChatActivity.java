package com.scorpionapps.dizmo;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class ChatActivity extends AppCompatActivity {
    TextToSpeech t1;
    LocationManager lcm;
    public String response;
    BufferedReader reader;
    HttpURLConnection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_chat);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
    }

    public void onSend(View v) throws InterruptedException {


        EditText editv = (EditText) findViewById(R.id.sendBox);
        final ScrollView scrv = (ScrollView) findViewById(R.id.scrollview1);
        TextView textView1 = new TextView(this);
        RelativeLayout rlv = (RelativeLayout) findViewById(R.id.relative);
        String msg = editv.getText().toString();
        GradientDrawable bgshape3 = (GradientDrawable) rlv.getBackground();
        LinearLayout scv = (LinearLayout) findViewById(R.id.linear_yout_scroll);
        LinearLayout linearlayout = (LinearLayout) findViewById(R.id.linear_layout);
        textView1.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT));

        textView1.setText("You : " + msg);

        textView1.setMaxLines(1);
        textView1.setMinWidth(250);
        textView1.setMinHeight(50);
        textView1.setTextSize(20);

        textView1.setBackgroundResource(R.drawable.shape); // hex color 0xAARRGGBB
        GradientDrawable bgShape1 = (GradientDrawable) textView1.getBackground();
        textView1.setTextColor(getResources().getColor(R.color.light));

        scv.addView(textView1);

        TextView sendButton = (TextView) findViewById(R.id.button_send);
        textView1.setId(textView1.generateViewId());
        TextView vartxt = (TextView) findViewById(textView1.getId());
        ViewGroup.MarginLayoutParams vartxt1 = (ViewGroup.MarginLayoutParams) vartxt.getLayoutParams();
        vartxt1.setMargins(25, 25, 0, 0);
        vartxt.setPadding(15, 15, 15, 15);
        vartxt.setGravity(Gravity.CENTER);
        String response = editv.getText().toString().toLowerCase();
        TextView textView2 = new TextView(this);
        textView2.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT));

        textView2.setBackgroundResource(R.drawable.shapesecond); // hex color 0xAARRGGBB
        GradientDrawable bgShape2 = (GradientDrawable) textView2.getBackground();
        String responset76 = "Sorry I didn't catch that \n" + "Try saying help to see available things you can try";
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        String weburl;
        textView2.setTextColor(getResources().getColor(R.color.light));
        scv.addView(textView2);

        textView2.getParent().requestChildFocus(textView2, textView2);
        String jokes[] = {"Why did the physics teacher break up with the biology teacher? There was no chemistry", "Can a kangaroo jump higher than a house? Of course, a house doesn’t jump at all.", "Some nice Chinese couple gave me a very good camera down by the Washington Monument. I didn’t really understand what they were saying, but it was very nice of them."};//Add the jokes here
        int joke = jokes.length;
        String wishes[] = {"Jai Hind", "Meraa Bhaarat Maahaan", response, "Same to you my friend"};
        int wish = wishes.length;
        String greetings[] = {"Hi", "Hello", "Namaste", "Hi there", "Hello User", "Namaste \n What can I do for you?"};//Add greetings here
        int greeting = greetings.length;
        String errors[] = {"Sorry I didn't understand \n Type Quick to quickview websites", "Sorry I didn't catch that \n Type Play to play games on the go", "Can you rephrase that? \n Want directions? Try typing directions from startPlace to endPlace", "I don't talk Gibberish", "Unfortunately I can't parse that information for now \n Try typing set theme to change the current theme", "Can you repeat that again?? \n Try typing set language to change language"};//Add error messages
        int error = errors.length;
        Random l = new Random();
        int o = l.nextInt(error);
        responset76 = errors[o];
        String start;
        String end;
        start = "Boomerang";
        end = "Andheri";
        Intent intent = new Intent(Intent.ACTION_CALL);
        GradientDrawable bgshape4 = (GradientDrawable) linearlayout.getBackground();
        if (response.contains("help".toLowerCase())) {
            responset76 = "What Help Do You Need?";
            ;
        } else if (response.contains("ambulance".toLowerCase())) {
            responset76 = "Sure , Calling Ambulance....";
            intent.setData(Uri.parse("tel:+918080706835"));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        } else if (response.contains("fire".toLowerCase())) {
            responset76 = "Sure , Calling Fire Truck....";
            intent.setData(Uri.parse("tel:+918169144847"));
            startActivity(intent);
        } else if (response.contains("police".toLowerCase())) {
            responset76 = "Sure , Calling Police....";
            intent.setData(Uri.parse("tel:+919702429678"));
            startActivity(intent);
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }




//source https://www.countries-ofthe-world.com/world-time-zones.html


        textView2.setText("T76 : "+responset76);


        textView2.setMinWidth(250);
        textView2.setMinHeight(50);
        textView2.setTextSize(20);
        t1.speak(responset76, TextToSpeech.QUEUE_FLUSH, null);


        textView2.setId(textView1.generateViewId());
        TextView vartxt3 =(TextView)findViewById(textView2.getId());
        ViewGroup.MarginLayoutParams vartxt2=(ViewGroup.MarginLayoutParams)vartxt3.getLayoutParams();
        vartxt2.setMargins(25,25,25,25);
        vartxt3.setPadding(15,15,15,15);
        vartxt3.setGravity(Gravity.CENTER);
        editv.setText("");

    }
    public void ifElse(){

    }
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }}
