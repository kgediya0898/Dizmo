package com.scorpionapps.dizmo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.klinker.android.send_message.Message;
import com.klinker.android.send_message.Settings;
import com.klinker.android.send_message.Transaction;

import java.util.ArrayList;
import java.util.Locale;

import jp.wasabeef.blurry.Blurry;

public class MainActivity extends AppCompatActivity {

    private static final int REQ_CODE_SPEECH_INPUT = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imgv=(ImageView)findViewById(R.id.support_view);

        imgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inntent = new Intent(getApplicationContext(),ChatActivity.class);
                startActivity(inntent);
            }
        });

    }
public void activitychange(View v){
        Intent inte = new Intent(getApplicationContext(),AboutUS_activity.class);
        startActivity(inte);

}
    public void getSpeech(View v) {
        startVoiceInput();
    }


    public void submit(View v) {


        Bundle bnd = new Bundle();
        if (v == findViewById(R.id.fire_view)) {
            bnd.putString("number", "+918169144847,Fire truck");
        } else if (v == findViewById(R.id.ambulance_view)) {
            bnd.putString("number", "+918080706835,Ambulance");
        } else if (v == findViewById(R.id.police_view)) {
            bnd.putString("number", "+919702429678,Police");
        } else if (v == findViewById(R.id.family_view)) {
            bnd.putString("number", "+919167536673");
        } else if (v == findViewById(R.id.support_view)) {

        }
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtras(bnd);
        startActivity(intent);

    }
    public void switchAct(View v){
        Intent intent = new Intent(this,ChatActivity.class);
        startActivity(intent);
    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent intent = new Intent(Intent.ACTION_CALL);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (result.get(0).contains("ambulance")) {
                        intent.setData(Uri.parse("tel:+918080706835"));
                    }else if(result.get(0).contains("police")){
                        intent.setData(Uri.parse("tel:+919702429678"));
                    }else if(result.get(0).contains("fire")||result.get(0).contains("rescue")||result.get(0).contains("fire")){
                        intent.setData(Uri.parse("tel:+918169144847"));
                    }


                        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                   }
                }
                break;
            }

        }
    }




