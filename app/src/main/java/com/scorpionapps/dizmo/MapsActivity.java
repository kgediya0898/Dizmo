package com.scorpionapps.dizmo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.klinker.android.send_message.Message;
import com.klinker.android.send_message.Settings;
import com.klinker.android.send_message.Transaction;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity


        implements GoogleMap.OnMyLocationButtonClickListener,GoogleMap.OnMyLocationClickListener,OnMapReadyCallback {


    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
// TODO: Before enabling the My Location layer, you must request
        // location permission from the user. This sample does not include
        // a request for location permission.
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
  mMap.setOnMyLocationButtonClickListener(this);
 mMap.setOnMyLocationClickListener(this);
}
@Override
 public void onMyLocationClick(@NonNull Location location) {
        String textToSend = "Latitiude :"+location.getLatitude() +" Longitude :" +location.getLongitude() ;
        String addressToSendTo = "+918169144847" ;
    Intent in=getIntent();
    Bundle b = in.getExtras();
    Geocoder geocoder;
    List<Address> addresses;
    geocoder = new Geocoder(this, Locale.getDefault());

    try {
        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
         // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        addressToSendTo= b.getString("number");
        String service[] = addressToSendTo.split(",");
        addressToSendTo=service[0];
        String address ="I need "+service[1]+" at " +addresses.get(0).getAddressLine(0);
        sendMsg(address,addressToSendTo);
        Toast.makeText(this, "Your Message Has Been Sent", Toast.LENGTH_SHORT).show();
    } catch (IOException e) {
        e.printStackTrace();
    }


        // Toast.makeText(this, "Current location:\n" + location.getLatitude(), Toast.LENGTH_LONG).show();
 }
 @Override
public boolean onMyLocationButtonClick() {
     Toast.makeText(this, "Tap on location to send message", Toast.LENGTH_SHORT).show();

// Return false so that we don't consume the event and the default behavior still occurs
 // (the camera animates to the user's current position).
 return false;
 }
 public void sendMsg(String textToSend,String addressToSendTo){
     Settings settings = new Settings();
     settings.setUseSystemSending(true);
     Transaction transaction = new Transaction(getApplicationContext(), settings);
     Message message = new Message(textToSend, addressToSendTo);

     transaction.sendNewMessage(message, Transaction.NO_THREAD_ID);
 }
        }
